import nationstates
import json

from xml_parser import CardDumpParser
from card_collection import CardCollection
from card import Card

NATION_FILE = "nationlist.txt"
CARDS_FILE = "cards.json"
S1_DUMP = "res/cardlist_S1.xml"
S2_DUMP = "res/cardlist_S2.xml"

if __name__ == '__main__':

    with open(NATION_FILE, 'r') as f:
        nations = f.readlines()


    """
    {
        "<cardid": [
            {
                "cardid": "<cardid>",
                "season": "<season>",
                "nations": {
                    "NavaerInc 082": 1,
                    "NavaerInc 720": 3
                }
            },
            {
                ...
            }
        ]
    }
    """

    for i in range(len(nations)):
        nations[i] = nations[i].strip()

    #nations = ["NavaerInc 156"]

    print(nations)

    api = nationstates.Nationstates("Card Trading Analysis")

    print("Importing S1 Card dump...") 
    cards_from_dump_s1 = CardDumpParser.xmldump_to_collection(S1_DUMP)
    print("Done")
    print("Importing S2 Card dump...")
    cards_from_dump_s2 = CardDumpParser.xmldump_to_collection(S2_DUMP)
    print("Done")


    cards = CardCollection()
    for nation_name in nations:
        #nation = api.nation(nation_name)
        #print(nation.name)
        print(nation_name)

        cards_api = api.cards()
        card_api_response = cards_api.decks(nation_name=nation_name)



        if "deck" in card_api_response and card_api_response["deck"] is not None:
            deck = card_api_response["deck"]["card"]
            print("Processing {} cards...".format(str(len(deck))))
            for deckcard_with_season in deck:
                cardid = deckcard_with_season["cardid"]
                season = deckcard_with_season["season"]
                card = None
                card_ref = None
                if season == '1':
                    if cardid in cards_from_dump_s1.cards:
                        card_ref = cards_from_dump_s1.cards[cardid][0]
                        card = Card(
                            cardid=card_ref.cardid, category=card_ref.category,
                            flag=card_ref.flag, govt=card_ref.govt, market_value=card_ref.market_value,
                            name=card_ref.name, region=card_ref.region, season=card_ref.season,
                            slogan=card_ref.slogan, type=card_ref.type, nations=dict()
                            )
                        card.add_nation(nation_name, count=1)
                        cards.add_card(card)
                    else:
                        print("Card {id} not found in S{season}".format(id=cardid, season=season))
                elif season == '2':
                    if cardid in cards_from_dump_s2.cards:
                        card_ref = cards_from_dump_s2.cards[cardid][0]
                        card = Card(
                            cardid=card_ref.cardid, category=card_ref.category,
                            flag=card_ref.flag, govt=card_ref.govt, market_value=card_ref.market_value,
                            name=card_ref.name,region=card_ref.region, season=card_ref.season,
                            slogan=card_ref.slogan, type=card_ref.type, nations=dict()
                            )
                        card.add_nation(nation_name, count=1)
                        cards.add_card(card)
                    else:
                        print("Card {id} not found in S{season}".format(id=cardid, season=season))
            print("Done")
            """
            for card in deck:
                card = cards_api.individual_cards(cardid=card["cardid"], season=card["season"], shards="info")
                card["nations"] = {nation_name: 1}
                if card["cardid"] in cards:
                    add_season = True  # Whether we have to add a new season
                    for card_with_season in cards[card["cardid"]]:
                        if card_with_season["season"] == card["season"]:
                            add_season = False
                            if nation_name in card_with_season["nations"]:
                                card_with_season["nations"][nation_name] += 1
                            else:
                                card_with_season["nations"][nation_name] = 1
                            break
                    if add_season:
                        cards[card["cardid"]].append(card)
                else:
                    cards[card["cardid"]] = [card]
            print(cards)
            """

        else:
            print("No card in {}".format(nation_name))

    print(len(cards))
    json_object = json.dumps(cards.to_dict(), indent = 4)
    
    # Writing to sample.json
    with open(CARDS_FILE, "w") as outfile:
        outfile.write(json_object)