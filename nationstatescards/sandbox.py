import xmltodict
import gzip

S1_DUMP = "cardlist_S1.xml.gz"

def handle_card(_, card):
    print(card['ID'])
    return True

xmltodict.parse(gzip.GzipFile(S1_DUMP), item_depth=2, item_callback=handle_card)