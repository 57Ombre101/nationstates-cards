import xml.sax
import string

from card import Card
from card_collection import CardCollection

"""
from xml.dom.minidom import parse
import xml.dom.minidom

S1_DUMP = "cardlist_S1.xml"
"""

class CardHandler( xml.sax.ContentHandler ):
    def __init__(self):
        self.CurrentData = ""
        self.id = ""
        self.name = ""
        self.type = ""
        self.motto = ""
        self.category = ""
        self.region = ""
        self.flag = ""
        self.card_category = ""
        self.description = ""
        self.badges = []
        self.trophies = []

    def startElement(self, tag, attributes):
        self.CurrentData = tag
        if tag == "CARD":
            print("===CARD===")

    def endElement(self, tag):
        if self.CurrentData == "ID":
            print("Id: {}".format(self.id))
        self.CurrentData = ""
    
    def characters(self, content):
      if self.CurrentData == "ID":
          self.id = content

#parser = xml.sax.make_parser()

# turn off namepsaces
#parser.setFeature(xml.sax.handler.feature_namespaces, 0)

# override the default ContextHandler
#Handler = CardHandler()
#parser.setContentHandler( Handler )


"""
DOMTree = xml.dom.minidom.parse(S1_DUMP)
collection = DOMTree.documentElement
cards = collection.getElementsByTagName("CARD")
"""

def begin_tag(tag: str) -> str:
    return "<{}>".format(tag)

def end_tag(tag: str) -> str:
    return "</{}>".format(tag)

def extract_from_tag(text, tag: str):
    l_len = len(begin_tag(tag))
    r_len = len(end_tag(tag))
    return text[l_len:-r_len] #.lstrip(begin_tag(tag)).rstrip(end_tag(tag))

class CardDumpParser():
    def __init__(self) -> None:
        pass

    @classmethod
    def xmldump_to_collection(cls, xml_file: str) -> CardCollection:
        tag_season = "SEASON"
        tag_card = "CARD"
        tag_id = "ID"
        tag_name = "NAME"
        tag_type = "TYPE"
        tag_motto = "MOTTO"
        tag_category = "CATEGORY"
        tag_region = "REGION"
        tag_flag = "FLAG"
        tag_cardcategory = "CARDCATEGORY"
        tag_description = "DESCRIPTION"

        """
        <BADGES>
        <BADGE>Admin</BADGE>
        <BADGE>Issues Editor</BADGE>
        <BADGE>WA</BADGE>
        <BADGE>Founder</BADGE>
        <BADGE>Moderator</BADGE>
        <BADGE>Easter Eggs (3)</BADGE>
        </BADGES>
        <TROPHIES>
        <TROPHY type="POPULATION-1">475</TROPHY>
        <TROPHY type="ECO-GOVT-5">2875</TROPHY>
        <TROPHY type="ENVIRONMENT-5">3923</TROPHY>
        </TROPHIES>
        """

        card_collection = CardCollection(dict())

        with open(xml_file, 'r', errors='ignore') as f:
            line = f.readline().strip(string.whitespace)
            card = None
            season = None
            while line:
                is_encoding_error = False
                try:
                    line = f.readline().strip(string.whitespace)
                    #print(line)
                except UnicodeDecodeError as e:
                    print("Error " + e.reason)
                    is_encoding_error = True

                if not(is_encoding_error):
                    if line.startswith(begin_tag(tag_season)):
                        season = extract_from_tag(line, tag_season)
                    elif line.startswith(begin_tag(tag_card)):
                        card = Card(season=season if season is not None else "")
                    elif line.endswith(end_tag(tag_card)):
                        card_collection.add_card(card)
                        card = None

                    elif line.startswith(begin_tag(tag_id)):
                        card.cardid = extract_from_tag(line, tag_id)
                    elif line.startswith(begin_tag(tag_cardcategory)):
                        card.category = extract_from_tag(line, tag_cardcategory)
                    elif line.startswith(begin_tag(tag_flag)):
                        card.flag = extract_from_tag(line, tag_flag)
                    elif line.startswith(begin_tag(tag_category)):
                        card.govt = extract_from_tag(line, tag_category)
                        # what about market value ?
                    elif line.startswith(begin_tag(tag_name)):
                        card.name = extract_from_tag(line, tag_name)
                    elif line.startswith(begin_tag(tag_region)):
                        card.region = extract_from_tag(line, tag_region)
                    elif line.startswith(begin_tag(tag_motto)):
                        card.slogan = extract_from_tag(line, tag_motto)
                    elif line.startswith(begin_tag(tag_category)):
                        card.region = extract_from_tag(line, tag_category)
        return card_collection

if __name__ == '__main__':
    S1_DUMP = "cardlist_S1.xml"
    S2_DUMP = "cardlist_S2.xml"
    TEST_DUMP = "testdump.xml"

    card_collection = CardDumpParser.xmldump_to_collection(TEST_DUMP)
    
    print(len(card_collection))
    for cardid in card_collection.cards:
        for card in card_collection.cards[cardid]:
            print("[{id}] {name} (S{season})".format(id=card.cardid, name=card.name, season=card.season, region=card.region))
    
