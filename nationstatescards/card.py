class Card():
    def __init__(
        self, cardid: str="", category: str="",
        flag: str="", govt: str="", market_value: str="", name: str="",
        region: str="", season: str="", slogan: str="", type: str="",
        nations: dict={}) -> None:
        self.cardid = cardid
        self.category = category
        self.flag = flag
        self.govt = govt
        self.market_value = float(market_value if market_value else "0")
        self.name = name
        self.region = region
        self.season = season
        self.slogan = slogan
        self.type = type
        self.nations = nations

    def __len__(self):
        res = 0
        for nation in self.nations:
            res += self.nations[nation]
        return res

    def add_nation(self, nation: str, count: int=1) -> None:
        """
        We assume self.nation is already a dict
        """
        if nation in self.nations:
            print("There is already {}".format(str(self.nations[nation])))
        else:
            print("No nation in nations")
        self.nations[nation] = self.nations[nation] + count if nation in self.nations else count

    def url(self):
        return "https://www.nationstates.net/page=deck/card=" + self.cardid + "/season=" + self.season
    
    def to_dict(self) -> dict:
        return {
            "cardid": self.cardid,
            "category": self.category,
            "flag": self.flag,
            "govt": self.govt,
            "market_value": self.market_value,
            "name": self.name,
            "region": self.region,
            "season": self.season,
            "slogan": self.slogan,
            "type": self.type,
            "nations": self.nations
        }

