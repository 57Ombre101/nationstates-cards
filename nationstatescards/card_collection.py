import json

from card import Card

class CardCollection():
    def __init__(self, carddict: dict=dict()) -> None:
        self.cards = dict()
        for cardid in carddict:
            if not(cardid in self.cards):
                self.cards[cardid] = []
            for card_with_season in carddict[cardid]:
                card = Card(cardid=card_with_season["cardid"], category=card_with_season["category"],
                flag=card_with_season["flag"], govt=card_with_season["govt"], market_value=card_with_season["market_value"], name=card_with_season["name"],
                region=card_with_season["region"], season=card_with_season["season"], slogan=card_with_season["slogan"], type=card_with_season["type"],
                nations=card_with_season["nations"])

                for card_with_season_present in self.cards[cardid]:
                    if card.season == card_with_season_present.season:
                        print("Card with season already exists")
                        for nation in card.nations:
                            if nation in self.cards[cardid].nations:
                                self.cards[cardid].nations[nation] += card.nations[nation]
                            else:
                                self.cards[cardid].nations[nation] = card.nations[nation]
                else:
                    self.cards[cardid].append(card)
    
    @classmethod
    def from_json(cls, json_file):
        with open(json_file, 'r') as f:
            cardsdict = json.load(f)
        return cls(cardsdict)

    def to_dict(self) -> dict:
        res = dict()
        for card_id in self.cards:
            if not card_id in res:
                res[card_id] = []
            for card in self.cards[card_id]:
                res[card_id].append(card.to_dict())
        return res
    
    def __len__(self):
        total = 0
        for card_id in self.cards:
            for card_with_season in self.cards[card_id]:
                for nation_name in card_with_season.nations:
                    total += card_with_season.nations[nation_name]
        return total
    
    def add_card(self, card: Card):
        if card.cardid in self.cards:
            if card.season in [card_with_season.season for card_with_season in self.cards[card.cardid]]:
                for card_with_season in self.cards[card.cardid]:
                    if card.season == card_with_season.season:
                        for nation in card.nations:
                            card_with_season.nations[nation] = card_with_season.nations[nation] + card.nations[nation] if nation in card_with_season.nations else card.nations[nation]
            else:
                self.cards[card.cardid].append(card)
        else:
            self.cards[card.cardid] = [card]

    def count_cards_in_nations(self, nations: list) -> int:
        total = 0
        for card_id in self.cards:
            for card_with_season in self.cards[card_id]:
                for nation_name in card_with_season.nations:
                    if nation_name in nations:
                        total += card_with_season.nations[nation_name]
        return total

    def count_cards_in_nation(self, nation: str):
        return self.count_cards_in_nations([nation])

    def get_cards_in_regions(self, regions: list) -> list:
        cards_res = list()
        for card_id in self.cards:
            for card_with_season in self.cards[card_id]:
                if card_with_season.region in regions:
                    cards_res.append(card_with_season)
        return cards_res

    def get_cards_in_region(self, region: str) -> list:
        return self.get_cards_in_regions([region])

    def rarity_stats(self):
        rarities = {
            "common": 0,
            "uncommon": 0,
            "rare": 0,
            "ultra-rare": 0,
            "epic": 0,
            "legendary": 0
        }
        for cardid in self.cards:
            for card in self.cards[cardid]:
                rarities[card.category] += len(card)
        return rarities
