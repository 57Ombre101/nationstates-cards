import unittest

from nationstatescards import Card

class TestCard(unittest.TestCase):
    def test_card_length(self):
        card = Card(
            cardid="1090050", category="common", flag="Default.png",
            govt="Iron Fist Consumerists", market_value="0.01",
            name="North Pacific Spy Agent 32", region="Lazarus",
            season="2", slogan="Mostly Harmless",
            type="Protectorate", nations={
                "NavaerInc 082": 6,
                "NavaerInc 057": 5,
                "NavaerInc 729": 65
        })
        self.assertEqual(len(card), 76)
    
    def test_card_to_dict(self):
        card = Card(
            cardid="1090050", category="common", flag="Default.png",
            govt="Iron Fist Consumerists", market_value="0.01",
            name="North Pacific Spy Agent 32", region="Lazarus",
            season="2", slogan="Mostly Harmless",
            type="Protectorate", nations={
                "NavaerInc 082": 6,
                "NavaerInc 057": 5,
                "NavaerInc 729": 65
        })
        dict_card = {
            "cardid": "1090050", "category": "common", "flag": "Default.png",
            "govt": "Iron Fist Consumerists", "market_value": 0.01,
            "name": "North Pacific Spy Agent 32", "region": "Lazarus",
            "season":"2", "slogan": "Mostly Harmless",
            "type": "Protectorate", "nations": {
                "NavaerInc 082": 6,
                "NavaerInc 057": 5,
                "NavaerInc 729": 65
                }
            }
        self.assertEqual(card.to_dict(), dict_card)

    def test_card_add_nation(self):
        
        card = Card(
            cardid="1090050", category="common", flag="Default.png",
            govt="Iron Fist Consumerists", market_value="0.01",
            name="North Pacific Spy Agent 32", region="Lazarus",
            season="2", slogan="Mostly Harmless",
            type="Protectorate")
        self.assertEqual(card.nations, dict())

        card.add_nation("NavaerInc 032")
        self.assertEqual(card.nations, {"NavaerInc 032": 1})

        card.add_nation("NavaerInc 837")
        self.assertEqual(card.nations, {"NavaerInc 032": 1, "NavaerInc 837": 1})

        card.add_nation("NavaerInc 793", 3)
        self.assertEqual(card.nations, {"NavaerInc 032": 1, "NavaerInc 837": 1, "NavaerInc 793": 3})

        card.add_nation("NavaerInc 032")
        self.assertEqual(card.nations, {"NavaerInc 032": 2, "NavaerInc 837": 1, "NavaerInc 793": 3})

        card.add_nation("NavaerInc 032", 5)
        self.assertEqual(card.nations, {"NavaerInc 032": 7, "NavaerInc 837": 1, "NavaerInc 793": 3})

if __name__ == '__main__':
    unittest.main()
