import unittest

from nationstatescards import CardCollection
from nationstatescards import Card

class TestCardCollection(unittest.TestCase):
    def test_card_collection_length(self):
        card_collection_n082 = CardCollection.from_json("tests/test_files/cards-navaerinc082.json")
        self.assertEqual(len(card_collection_n082), 11)

        card_collection_001 = CardCollection.from_json("tests/test_files/cards-001.json")
        self.assertEqual(len(card_collection_001), 1+1+3+42+3+6+5+65)

    def test_card_collection_nation_count(self):
        card_collection_001 = CardCollection.from_json("tests/test_files/cards-001.json")
        self.assertEqual(card_collection_001.count_cards_in_nation("NavaerInc 082"), 1+1+6)
        self.assertEqual(card_collection_001.count_cards_in_nation("NavaerInc 057"), 3+42+5)
        self.assertEqual(card_collection_001.count_cards_in_nation("NavaerInc 729"), 3+65)
        self.assertEqual(card_collection_001.count_cards_in_nations(["NavaerInc 729", "NavaerInc 082"]), 3+65+1+1+6)

    def test_card_collection_add_card(self):
        card_collection_test = CardCollection(dict())
        card_001 = Card(cardid="001", region="Lazarus", name="Zoig", nations={"NaverInc 007": 1})
        card_collection_test.add_card(card_001)
        self.assertEqual(len(card_collection_test), 1)
        self.assertEqual(card_collection_test.cards[card_001.cardid][0].cardid, card_001.cardid)
        self.assertEqual(card_collection_test.cards[card_001.cardid][0].nations, {"NaverInc 007": 1})

        card_053 = Card(cardid="053", region="Lazarus", name="Ozjngrr", nations={"NaverInc 007": 1})
        card_collection_test.add_card(card_053)
        self.assertEqual(len(card_collection_test), 2)
        self.assertEqual(card_collection_test.cards[card_053.cardid][0].cardid, card_053.cardid)
        self.assertEqual(card_collection_test.cards[card_053.cardid][0].nations, {"NaverInc 007": 1})

    def test_card_collection_get_cards_in_regions(self):
        card_collection_001 = CardCollection.from_json("tests/test_files/cards-001.json")
        cards_in_regions = card_collection_001.get_cards_in_regions(["Lazarus", "South Pacific"])
        res_set = {card.name for card in cards_in_regions}
        self.assertEqual(res_set, {"Claws", "Liberaltopia", "North Pacific Spy Agent 32"})
    
    def test_card_collection_get_cards_in_region(self):
        card_collection_001 = CardCollection.from_json("tests/test_files/cards-001.json")
        cards_in_region = card_collection_001.get_cards_in_region("Lazarus")
        res_set = {card.name for card in cards_in_region}
        self.assertEqual(res_set, {"Liberaltopia", "North Pacific Spy Agent 32"})
    
    def test_card_collection_rarity_stats(self):
        card_collection_001 = CardCollection.from_json("tests/test_files/cards-001.json")
        self.assertEqual(card_collection_001.rarity_stats(), {
            "common": 1+6+5+65,
            "uncommon": 1+3,
            "rare": 42+3,
            "ultra-rare": 0,
            "epic": 0,
            "legendary": 0
        })
    
    def test_card_collection_to_dict(self):
        card = Card(
            cardid="1090050", category="common", flag="Default.png",
            govt="Iron Fist Consumerists", market_value="0.01",
            name="North Pacific Spy Agent 32", region="Lazarus",
            season="2", slogan="Mostly Harmless",
            type="Protectorate", nations={
                "NavaerInc 082": 6,
                "NavaerInc 057": 5,
                "NavaerInc 729": 65
        })

        card_collection = CardCollection()
        card_collection.add_card(card)

        dict_card_collection = { "1090050": [{
            "cardid": "1090050", "category": "common", "flag": "Default.png",
            "govt": "Iron Fist Consumerists", "market_value": 0.01,
            "name": "North Pacific Spy Agent 32", "region": "Lazarus",
            "season":"2", "slogan": "Mostly Harmless",
            "type": "Protectorate", "nations": {
                "NavaerInc 082": 6,
                "NavaerInc 057": 5,
                "NavaerInc 729": 65
                }
            }]
        }

        self.assertEqual(card_collection.to_dict(), dict_card_collection)


if __name__ == '__main__':
    unittest.main()
